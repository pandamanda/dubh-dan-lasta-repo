label ch1:

    ### Quest Progression Variables for Chapter 1 ###

    $ istal_progress = 0
    $ kincir_progress = 0
    $ kurir_progress = 0
    $ mentor_progress = 0

    #####

    "Sersan" "Hasil tes kekuatan dan kemampuan tempurmu bagus."
    "Sersan" "Terutama--di luar dugaan--untuk pertarungan tangan kosong."
    "Sersan" "Kau memenuhi semua persyaratan magang Guardian."
    "Sersan" "Masalahnya .... Genusmu ...."
    dub normal "Felis, Pak. Genus saya Felis."
    "Sersan" "Aku tahu, Nak. Terlihat amat sangat jelas."
    "Sersan" "{size=15}Sesuai hasil tesmu, kau tidak pintar-pintar amat, ya?{/size}"
    "Sersan" "Nah. Memang tidak tertulis kriteria bahwa seorang Guardian harus berasal dari Genus tertentu."
    "Sersan" "Tapi sudah berpuluh-puluh tahun anggota Guardian adalah para Canis dan Panthera."
    "Sersan" "Nah. Aku benar-benar minta maaf, tapi aku tak bisa meneruskan lamaran magangmu sebagai Guardian."
    dub sad "Tapi .... Kenapa?"
    "Sersan" "Maafkan kami, Nak. Ini demi kebaikanmu sendiri."
    dub sad "...."
    "Itu bukan jawaban yang Dubh harapkan, tapi melihat ekspresi wajah Sersan, Dubh tahu ia tidak bisa membantah."
    "Walaupun enggan, Dubh melangkah pergi meninggalkan ruangan Sersan tanpa mengatakan apapun lagi."
    "Menjadi Guardian adalah satu-satunya yang terpikir olehnya saat ulang tahunnya yang kelima belas."
    "Memang tidak biasanya Felis seperti Dubh mendaftar magang sebagai Guardian, tapi Dubh pikir ia punya kesempatan."
    "Ternyata ia ditolak karena jajaran Guardian didominasi oleh Canis dan Panthera."
    "{i}Tidak adil.{/i}"
    "Seperti kata Sersan, tidak ada peraturan yang mengharuskan seorang Guardian berasal dari Genus tertentu. Dubh pernah melihat satu-dua Vulpes dan Rattus."
    "Entah Rattus entah Mus, Dubh tidak yakin. Sulit membedakan keduanya."
    "{i}Benar-benar tidak adil.{/i}"

    scene bg persimpangan with fade
    play music tavern fadein 5.0
    "Tapi biarpun Dubh saat ini enggan pulang ke rumahnya, ia tak punya tujuan lain selain rumah."
    "Sekelebat bayangan merah menghampiri begitu Dubh melangkah melewati gerbang utama kota."
    show lasta normal at mcenter with dissolve
    "Itu Lasta, si Vulpes. Salah satu teman baik Dubh."
    "Ralat. Lasta mungkin adalah teman {i}terbaik{/i} Dubh, mengingat sifat Dubh yang pendiam dan cenderung mudah gugup."
    show lasta normal at midright with move
    show dubh normal at midleft with dissolve
    las sad "Tidak berhasil?"
    dub sad "Kelihatan sekali ya?"
    las "Yap."
    las normal "Kalau berhasil, kau pasti sudah melesat pulang ke rumah, bukan berjalan pelan seperti ini."
    las "Tentu saja aku tidak bakalan bisa menyusulmu kalau kau berlari begitu."
    "Dubh selalu kagum pada pengamatan Lasta."
    "Vulpes itu seperti selalu tahu siapa akan berbuat bagaimana dalam situasi apa."
    "Lasta selalu bilang itu bukan sesuatu yang hebat dan banyak yang bisa melakukan lebih baik darinya, tapi Dubh tetap kagum."


    dub normal "Ujian Lasta bagaimana?"
    dub "Ujian Scout, maksudku."
    "Selain Guardian, Scout adalah satu dari Empat Profesi Terhormat di Kerajaan Karnessia."
    "Jika Guardian bertugas menjaga kota-kota di wilayah kerajaan dan perbatasan, Scout bertugas di hutan-hutan dan area dekat daerah Antah Berantah."
    "Seorang Guardian kuat dan terlihat mencolok dengan pelindung logam mereka, sementara Scout gesit dan membaur dengan lingkungan sekitar mereka."

    las sad "Mereka memintaku merapal Mantera."
    dub hesitated "Oh."
    las "Mereka bilang akan menghubungiku lagi, tapi kurasa aku gagal."
    "Karena satu dan lain hal, Lasta tidak bisa merapal Mantera, yang sebenarnya merupakan salah satu persyaratan menjadi Scout."
    "Tidak {i}selalu{/i} bisa, lebih tepatnya."
    dub normal "Mau mencoba melamar jadi Guardian?"
    las surprised "Eh, kurasa tidak."
    las "Membayangkan diriku pakai zirah rasanya tidak cocok."
    las "Sepertinya panas dan berat."
    dub "Mereka punya pelindung ringan dari kulit."
    las upset "Tidak usah. {i}Terima kasih banyak.{/i}"
    las "Pelindung ringan itu seperti amanat untuk cepat mati."
    las normal "Bagaimana kalau kau mencoba melamar ke Scout?"
    dub sad "Mereka punya kriteria tinggi badan maksimal."

    "Scout mengutamakan Genus yang mayoritas berbadan kecil dan bisa bergerak cepat, seperti Felis, Rattus dan Mus, Vulpes, dan beberapa varian Canis."
    "Dubh diolok-olok sebagai \"Panthera Palsu\" bukan tanpa sebab: ia jangkung dan kuat seperti Panthera remaja."
    "Scout tidak membutuhkan Felis seperti Dubh."
    "Itu sebabnya Dubh mendaftar sebagai Guardian, hanya untuk ditolak karena Genusnya."
    las sad "Waktu kaubilang kau gagal, hasil tesmu yang mana yang kurang?"
    dub hesitated "Tidak ada."
    las surprised "He?"
    dub "Mereka menolakku karena aku Felis. Katanya sudah bertahun-tahun Guardian diisi oleh Canis dan Panthera."
    las sad "Oh ...."
    dub sad "Tapi ada Guardian Vulpes dan Rattus."
    las surprised "Iya, ya?"
    "Kalau Lasta merespons seperti itu, Dubh jadi meragukan ingatannya sendiri."
    "Waktu itu dia melihat Vulpes dan Rattus mengenakan zirah, jadi Dubh pikir mereka adalah Guardian."
    las normal "Tapi kurasa Sersan bermaksud baik."
    dub normal "Sersan bilang ini untuk kebaikanku sendiri."
    las upset "Aku sejujurnya khawatir bagaimana para Panthera dan Canis memperlakukan Felis yang menjadi Guardian."
    dub sad "?"
    dub normal "Sama saja seperti yang lainnya?"
    "Alih-alih menjawab, Lasta hanya mengangkat bahu."
    hide lasta with dissolve
    hide dubh with dissolve

    "Hamparan ladang gandum yang sudah mulai menguning terbentang di sisi kiri-kanan jalan yang dilalui Dubh dan Lasta."
    "Sebentar lagi musim panen.{w} Dubh dan Lasta biasanya membantu memanen gandum-gandum tersebut."
    "Pekerjaan yang sebetulnya menyenangkan, namun Dubh tak yakin ia ingin menghabiskan hidupnya sebagai petani."
    "Bukan karena ia percaya ada takdir besar menantinya atau semacamnya. Dubh hanya merasa ... entahlah."
    "Ia ingin {i}bertarung{/i}."
    "Saat ia kecil, Dubh pernah diajak diam-diam menonton pertandingan tinju oleh ayahnya."
    "Tentu saja tanpa persetujuan sang ibu. Menurut Ibu, tidak pantas anak perempuan menonton baku hantam."
    "Seandainya larangan itu dipatuhi, Dubh mungkin tak akan memikirkan cita-cita lain selain meneruskan pertanian orang tuanya."
    "Ada sesuatu yang luar biasa dalam sebuah pertarungan. Sesuatu yang ... {i}menggugah{/i}."
    "...."
    "Apa Dubh sebaiknya tetap bertani, tapi diam-diam mengikuti pertandingan-pertandingan tinju itu?"
    "Ayah bilang para petinju itu mendapat uang juga, meskipun bagaimana persisnya, Dubh tidak paham."
    show lasta surprised at midright with dissolve
    las "Dubh, jangan bengong. Rumahmu di sebelah sana ...."
    show dubh hesitated at midleft with dissolve
    dub "Oh. Maaf."
    las normal "Ngomong-ngomong, aku baru saja dapat ide brilian soal lamaran magang."
    dub normal "Ide ... apa?"
    las smirk "Kita melamar magang sebagai Scavenger!"
    hide lasta with dissolve
    hide dubh with dissolve
    stop music fadeout 3.0

    scene black with fade

    "{i}Melamar menjadi Scavenger.{/i}"
    "Dubh mengulangi kalimat itu dalam benaknya sekali lagi."
    "Rasanya ... asing."
    "Sama asingnya seperti ayam berkaki empat. Atau domba berkaki enam. Atau gandum berwarna ungu."
    "Tapi Dubh menyukai ide tersebut."
    "{i}Kenapa?{/i}"
    "Dubh sendiri tidak tahu. Mungkin karena Lasta yang mengutarakannya?"
    "Saat makan malam, Dubh menceritakan soal ajakan Lasta pada kedua orang tuanya."
    "Ayahnya setuju-setuju saja. Ibunya ragu-ragu."
    "Terutama karena ujian magang untuk Scavenger hanya dilangsungkan di Ibukota. Dubh selama ini belum pernah pergi keluar dari kota kecil ini."
    "{i}Melamar menjadi Scavenger.{/i}"
    "Sesaat sebelum tidur, Dubh mengulangi kalimat itu kembali. Ucapan ibunya ketika makan malam tadi langsung menyusul."
    "{i}Kau yakin itu yang kau inginkan?{/i}"
    "{i}Bukan karena kau hanya ikut-ikutan apa kata Lasta?{/i}"

    "Dubh bangun sebelum matahari terbit."
    "Ia membasuh wajah dan merapikan bulunya yang tidak pernah tidak berantakan saat bangun tidur."
    "Setelah itu, sebelum turun ke bawah, Dubh melompat, meraih kasau di langit-langit kamarnya."
    "Ini salah satu latihan fisik yang pernah Dubh lihat dari Canis yang tinggal di daerah barat wilayah pertanian."
    "Canis itu hendak menjadi Guardian dan ia berlatih dengan palang yang didirikan di samping rumahnya."
    "Caranya mudah, jadi Dubh bisa langsung menirunya: berpegangan pada tiang melintang, lalu angkat badan dan turunkan perlahan."
    "Dubh pernah memberanikan diri bertanya pada si Canis, berapa banyak ia perlu melakukan itu dalam sehari?"
    "Katanya, sebanyak yang kau bisa, mulai saja dari sepuluh kali."
    "Sepuluh kali awalnya sulit."
    "Sekarang Dubh bisa sepuluh kali sepuluh kali. Kira-kira. Kalau Dubh tidak bengong dan lupa melanjutkan hitungannya."
    "Setelah rasanya sudah mencapai hitungan itu, Dubh turun ke bawah."
    "Ayah dan Ibu sama-sama sudah ada di dapur."

    scene bg rumahdubh with fade
    play music market fadein 5.0
    show dubh normal at mcenter with dissolve
    "Ayah" "Aaa, selamat pagi, Nak! {w}Sini!"
    "Ayah berjinjit dan mengacak-acak rambut Dubh."
    show dubh hesitated with dissolve
    "Entah kenapa Ayah senang sekali melakukan itu."
    "Meskipun sebenarnya tidak senang rambutnya jadi berantakan lagi, tapi Dubh selalu sengaja menunduk supaya Ayah bisa menjangkau kepalanya."
    show dubh normal with dissolve
    "Ayah" "Ayah pergi dulu kalau begitu."
    "Ayah" "Hari ini si pedagang datang. Ayah janji menemuinya di lumbung."
    "Ayah" "Kau harus ke rumah Mentor untuk minta surat rekomendasi, kan?"
    dub "I-iya. Surat rekomendasi."
    "Biasanya Dubh menemani sang ayah ke lumbung jika pembeli panen mereka datang."
    "Selain belajar bagaimana Ayah mengatur penjualan dan menanggapi tawaran, Dubh banyak membantu memindahkan barang."
    "Kalau Ayah bilang soal Mentor, itu artinya Dubh kali ini tidak perlu ikut ...."

    "Ayah" "Aku pergi dulu kalau begitu."
    "Ayah" "Semoga lancar, Nak!"
    dub smile "Iya. {w}Dadah."
    "Segera setelah Ayah pergi dan pintu ditutup, Dubh langsung menoleh ke arah ibunya."
    "Ibu menghela napas, kentara sekali bahwa ia punya banyak hal untuk dibicarakan dengan Dubh."
    "Hampir pasti terkait soal rencana Dubh mendaftar magang sebagai Scavenger."
    "Ibu" "Dubh."
    dub normal "Ya?"
    "Ibu" "Kau {i}benar-benar{/i} yakin mau pergi melamar magang sebagai Scavenger?"
    dub sad "Lasta mengajakku ...."
    "Ibu" "Kau tahu kan kau boleh menolak ajakannya?"
    "Ibu" "Selama ini Ibu tidak keberatan mengikuti Lasta, tapi kali ini ...."
    "Ibu" "Ini soal pekerjaan yang mungkin akan kau jalani sampai tua nanti."
    "Ibu" "Bukan sesuatu yang seharusnya kau putuskan dengan mengikuti orang lain. Paham, kan?"
    "Dubh mengangguk."
    "Ibu" "Jadi ... ya. Apa kau sudah yakin?"
    "Ibu" "Tabunganmu harusnya cukup untuk pergi ke ibukota, ke Isbellum. Ibu dan Ayah juga bisa memberimu tambahan, meskipun tidak banyak."
    "Ibu" "Tapi, ya .... Isbellum cukup jauh dan ...."
    "Ibu khawatir, Dubh sadar betul itu."
    "Dubh sebenarnya tidak punya ambisi berapi-api untuk menjadi Scavenger. Lasta jauh lebih bersemangat untuk itu."
    "Namun Dubh tak ingin menjadi petani. Jika menjadi seorang Scavenger adalah pilihan terakhirnya sebelum ia melanjutkan pekerjaan Ayah, Dubh ingin setidaknya mencoba."
    dub normal "Kalau aku gagal, aku akan pulang."
    "Ibu" "... Ya, sebaiknya kau pulang."
    "Ibu" "Ibu kasihan pada Lasta kalau dia harus mengasuhmu sampai entah kapan di Isbellum kalau kau tidak pulang."
    "Ibu" "Kalau gagal, sebaiknya kau pulang. Jangan menyusahkan Lasta."
    dub sad "I-iya!"
    "Ibu" "...."
    "Ibu" "Sebenarnya Ayah titip pesan. Ia tidak ingin mengatakan ini langsung padamu, entah kenapa."
    "Ibu" "Kata Ayah, lupakan sejenak soal meneruskan pertanian keluarga kita."
    "Ibu" "Dia ingin membiarkanmu melihat dunia, mengunjungi berbagai tempat di Karnessia, setidaknya."
    "Ibu" "Seperti kakek-kakek kami."
    "Dubh pernah mendengar sekilas bahwa kakek Ayah dan Ibu berasal dari suatu tempat yang jauh. Dari hutan dingin di pegunungan utara jauh, kalau tidak salah."
    "Tidak terlalu jelas apa yang membuat para kakek bepergian hingga mencapai tempat ini dan akhirnya menetap."
    dub normal "Ayah bilang begitu?"
    "Ibu" "Ya."
    "Ibu" "Mungkin ada sebagian kecil dalam diri Ayah yang sebenarnya ingin bebas dari kota kecil ini, tapi ada yang mengikatnya. Jadi ia ingin memberimu kesempatan."
    "Ibu" "... Tentu saja kami harap kau tidak ceroboh dan lebih berhati-hati di luar sana."
    "Dubh mengangguk."
    "Ibu" "Sana. Ambil tasmu dan siap-siap."
    "Ibu" "Sebentar lagi Lasta pasti datang."

    #knock knock knock
    play sound [thud, thud, thud]
    "Ibu" "Apa kubilang."
    hide dubh with moveoutleft
    "Sementara Ibu membukakan pintu, Dubh pergi mengambil tasnya di kamar."
    "Si Vulpes nampak berseri-seri saat Dubh menemuinya di bawah."
    show lasta smirk at midright with dissolve
    show dubh normal at midleft with dissolve
    las "Sudah siap?"
    "Dubh mengangguk."
    las normal "Aku akan menjaga Dubh seperti biasa, Bibi, jangan khawatir."
    las "Kami pergi dulu!"
    hide lasta with moveoutright
    show dubh at mcenter with move
    "Ibu" "Oh iya, Dubh?"
    "Ibu" "Kalau tidak salah kemarin kau bilang kau membantu di penggilingan dan di istal."
    dub hesitated "Oh. Iya. {w}Belum dibayar."
    "Ibu" "Tagih mereka. Semua orang desa tahu mereka berdua senang menunda-nunda begitu."
    dub normal "Iya."
    dub smile "Aku pergi dulu."
    "Ibu" "Hati-hati di jalan."
    hide dubh with moveoutright

    scene bg persimpangan with fade
    show lasta normal at midright with dissolve
    show dubh normal at midleft with dissolve
    las "Oke, jadi hari ini kita harus:"
    las "1. Pergi menemui Mentor minta surat rekomendasi{p}2. Menagih hutang gaji kerjamu"
    dub sad "Itu belum bisa disebut hutang ...."
    las upset "Oh, itu {i}hutang{/i}, jelas."
    las "Aku membantu di tukang roti dan dia selalu membayarku sebelum aku pulang.{w} Setiap hari."
    las "Kenapa sih si pemilik kincir dan pengurus istal pelit begitu?"
    las normal "Terakhir, kita perlu mencari tumpangan ke Isbellum."
    dub normal "Kita akan menumpang?"
    las "Hmm, iya."
    las "Ada jasa kereta ke Isbellum, tapi kupikir bisa lebih hemat kalau menumpang."
    las "Tapi sebetulnya aku tidak tahu kapan ada pedagang atau kurir yang kemari kemudian menuju ibukota."
    dub "Ayah bilang hari ini dia akan menemui pedagang yang biasa membeli hasil panennya."
    dub "Orang itu dari ibukota. Biasanya dia bersama rombongan kurir dan pedagang lain."
    las surprised "Ohhhh! {w}Kita coba menemui salah satu dari mereka kalau begitu."
    las normal "Baiklah. Ayo kita mulai."
    las "Aku ikut kau mau ke mana duluan."
    hide lasta with dissolve
    hide dubh with dissolve

    $ curr_location = "persimpangan"
    show screen map_btn("ch1_map")
    #call screen 



##### MAP SECTION OF CHAPTER 1 #####
label persimpangan:
    $ curr_location = "persimpangan"
    call screen ch1_persimpangan

label rumahdubh:
    $ curr_location = "rumah dubh"
    call screen ch1_rumahdubh

label kincir:
    $ curr_location = "kincir"
    call screen ch1_kincir

label istal:
    $ curr_location = "istal"
    call screen ch1_istal

label rumahmentor:
    $ curr_location = "rumah mentor"
    call screen ch1_rumahmentor

label toko:
    $ curr_location = "toko"
    call screen ch1_toko


label ch1_map:
    menu:
        "Rumah Dubh" if curr_location != "rumah dubh":
            scene bg rumahdubh with fade
            if istal_progress == 999 and kincir_progress == 999 and mentor_progress == 999 and kurir_progress == 999:
                jump endch1
            else:
                jump rumahdubh
        "Kincir Angin" if curr_location != "kincir":
            scene bg kincir with fade
            jump kincir
        "Istal" if curr_location != "istal":
            scene bg istal with fade
            jump istal
        "Rumah Mentor" if curr_location != "rumah mentor":
            scene bg rumahmentor with fade
            jump rumahmentor
        "Toko" if curr_location != "toko":
            scene bg toko with fade
            jump toko
        "Persimpangan Desa" if curr_location != "persimpangan":
            scene bg persimpangan with fade
            jump persimpangan

screen ch1_istal:
    imagebutton:
        xanchor 0.5
        yanchor 0.5
        xpos 400
        ypos 500
        idle "element_green_diamond.png"
        focus_mask True
        action Jump("istal_text")

screen ch1_kincir:
    imagebutton:
        xanchor 0.5
        yanchor 0.5
        xpos 200
        ypos 300
        idle "element_green_diamond.png"
        selected_idle "element_green_diamond.png"
        action Jump("kincir_text")

    imagebutton:
        xanchor 0.5
        #yanchor 0.5
        xpos .75
        ypos 1
        idle "placeholdersprites.png"
        selected_idle "placeholdersprites.png"
        action Jump("petani_text")

screen ch1_rumahmentor:
    imagebutton:
        xanchor 0.5
        yanchor 0.5
        xpos 500
        ypos 400
        idle "element_green_diamond.png"
        selected_idle "element_green_diamond.png"
        action Jump("mentor_text")

screen ch1_toko:
    imagebutton:
        xanchor 0.5
        yanchor 0.5
        xpos 200
        ypos 300
        idle "element_grey_diamond.png"
        selected_idle "element_grey_diamond.png"
        action Jump("toko_text")

# Notes: check istal_progress, kincir_progress, and mentor_progress to show Kurir at Village's Road
screen ch1_persimpangan:
    if kincir_progress == 999 and istal_progress == 999 and mentor_progress == 999:
        imagebutton:
            xanchor 0.5
            yanchor 0.5
            xpos 200
            ypos 300
            idle "element_green_diamond.png"
            selected_idle "element_green_diamond.png"
            action Jump("kurir_text")

# Notes: check kurir_progress, istal_progress, kincir_progress, and mentor_progress to show Mother at Dubh's house
screen ch1_rumahdubh:
    if kincir_progress == 2:
        imagebutton:
            xanchor 0.5
            yanchor 0.5
            xpos 300
            ypos 300
            idle "element_green_diamond.png"
            selected_idle "element_green_diamond.png"
            action Jump("minyak_text")


##########

##### POINT AND CLICK CUTSCENE OF CHAPTER 1 #####
#Interact with windmill
label kincir_text:
    if kincir_progress == 0 or kincir_progress == 999:
        "Kincir angin untuk menggiling gandum. Tua, namun masih berfungsi."
    elif kincir_progress == 1:
        $ kincir_progress = 2
        las upset "Hmm .... Tuasnya memang berat sekali."
        dub normal "Coba kudorong lebih keras."
        las surprised "Oh. Jangan!"
        las "Kalau patah malah tidak bisa dipakai lagi."
        las "Coba aku periksa giginya."
        "...."
        "...."
        las upset "Roda giginya berkarat dan tidak ada pelumasnya."
        las "Dubh, di rumahmu ada minyak bekas?"
        dub normal "Aku tahu di mana Ibu menyimpannya di dapur. Ayah memakainya untuk engsel pintu lumbung dan pagar."
        las normal "Ambil sedikit saja kalau boleh dan bawa kemari."
        dub smile "Oke"
    elif kincir_progress == 2:
        "Roda gigi tuas kincir angin berkarat dan perlu diminyaki."
    elif kincir_progress == 3:
        $ kincir_progress = 4
        dub normal "Cukup?"
        las smirk "Yap, terima kasih."
        las "Sebentar."
        "...."
        "...."
        las normal "Oke, selesai."
        las "Coba geser tuasnya, Dubh. Jangan terlalu bertenaga."
        "Dubh menggerakkan tuas."
        "Masih terasa berat, tapi tidak seberat tadi."
        "Diiringi bunyi berderak menakutkan, tuas bergeser, dan roda-roda gigi mulai berputar."
        dub smile "Berhasil."
        las smirk "Yap."
        las "Tos dulu."
        dub sad "Tangan Lasta berminyak."
        las surprised "Oh. Benar juga. Maaf, maaf."

    jump kincir

#Speak with farmer/windmill owner
label petani_text:
    if kincir_progress == 0:
        $ kincir_progress = 1
        "Petani" "Ya?"
        dub hesitated "Ng .... Aku ...."
        dub "Bayaran ... ku?"
        "Petani" "Bayaran apa?"
        dub sad "Bayaran membantu kemarin ...."
        "Petani" "Oh. Itu."
        "Petani" "Jangan sekarang. Aku sedang pusing karena tuas kincir anginku macet tidak bisa bergerak."
        "Petani" "Kembali lagi saja besok!"
        las upset "Ck, dia sengaja ya?"
        las "Coba kita lihat ada masalah apa dengan tuas kincir anginnya."
    elif kincir_progress == 1:
        "Petani" "Ambil bayaranmu besok saja kalau mau. Kincir anginku macet, aku pusing."
    elif kincir_progress == 4:
        $ kincir_progress = 999
        dub normal "Kincirnya sudah betul."
        "Petani" "!!!"
        "Petani" "Tapi ...."
        "Petani" "Ehem! Tapi aku tidak memintamu membetulkannya, jadi jangan minta bayaran tambahan!"
        "Petani" "Kuambilkan uangnya."
        las surprised "Wow. Dia menyebalkan. Kenapa sih kau bekerja padanya, Dubh?"
        dub hesitated "Karena ... dibayar."
        las upset "Dia mencari alasan untuk tidak membayarmu, minimal menundanya!"
        dub sad "Ibu sudah mengingatkanku soal itu .... Katanya semua orang di sini juga sudah tahu."
        dub "Tapi kasihan kalau tidak ada yang membantunya sama sekali ...."
        las sad "Oh, Dubh. Kamu terlalu baik."
        "Petani" "Ini bayaranmu. Dua puluh Mur."
        dub smile "Terima kasih."
    elif kincir_progress == 999:
        "Petani" "Kenapa kalian masih di sini???"
        "Petani" "Sana pergi kalau sudah tidak ada urusan denganku!"
    
    jump kincir

#Speak with stable's owner
label istal_text:
    if istal_progress == 0:
        $ istal_progress = 1
        "Peternak" "... Ada perlu apa?"
        dub hesitated "Um ... bayaranku kemarin ...."
        "Peternak" "... Bisa datang besok saja?"
        "Peternak" "Catatan penjualanku hilang dan aku bingung menghitung hasil penjualanku kemarin."
        las normal "Ooh, memangnya kemarin ini Anda menjual apa saja?"
        "Peternak" "... Banyak."
        "Peternak" "Satu kuda hitamku terjual seharga 500 Mur. Lalu ada yang membeli tiga karung pupuk dariku. Masing-masing karung harganya 50 Mur."
        "Peternak" "Aku menyimpan tapal kuda bekas dan kemarin juga ada yang membelinya. Totalnya 10, masing-masing dihargai 5 Mur."
        "Berapa Mur total penghasilan hari itu?"
        menu:
            "500 Mur":
                jump istal_wrong_answer
            "600 Mur":
                jump istal_wrong_answer
            "700 Mur":
                jump istal_right_answer

    elif istal_progress == 1:
        las normal "Bisa tolong ulangi kemarin Anda menjual apa saja?"
        "Peternak" "Satu kuda hitam seharga 500 Mur. Lalu tiga karung pupuk dariku. Masing-masing harganya 50 Mur."
        "Peternak" "Kemudian tapal kuda bekas. Ada sepuluh, tiap tapal kuda dihargai 5 Mur."
        "Berapa Mur total penghasilan hari itu?"
        menu:
            "500 Mur":
                jump istal_wrong_answer
            "600 Mur":
                jump istal_wrong_answer
            "700 Mur":
                jump istal_right_answer

    elif istal_progress == 999:
        "Peternak" "Sudah kan? Kalian sudah tidak ada urusan lagi denganku kan? Sana."
    
    jump istal

#When wrong answer at the stable
label istal_wrong_answer:
    dub sad "Lasta?"
    las surprised "Maaf. Aku melewatkan sesuatu."
    las "Biar kucoba lagi."
    
    jump istal

#When right answer at the stable
label istal_right_answer:
    $ istal_progress = 999
    las normal "Anda dapat 700 Mur kemarin dari menjual semua itu."
    las smirk "Saya rasa lebih dari cukup untuk membayar teman saya."
    "Peternak" ".... {w}Dasar Vulpes lancang."
    "Peternak" "Yah. Terserah."
    "Peternak" "Ini bayaranmu. Seratus Mur."
    dub smile "Terima kasih."

    jump istal

#Speak with shop owner
label toko_text:
    if kurir_progress == 1:
        "Penjaga Toko" "Selamat datang! Ada yang bisa kubantu?"
        dub hesitated "Kami ... mau mengambil pesanan ...."
        dub "Pesanan Tiomanach ...."
        "Penjaga Toko" "Ohh, Tio."
        "Penjaga Toko" "Hmm, apa saja pesanannya?"
        "Apa pesanan si kurir?"

        menu:
            "Air bersih":
                jump toko_question_two
            "Madu":
                jump toko_wrong_answer
            "Bir":
                jump toko_wrong_answer

    else:
        "Penjaga Toko" "Selamat datang di Toko Supercal!"
    jump toko

#Second question at the shop
label toko_question_two:
    "Penjaga Toko" "Berapa kantung air bersih yang dia minta?"

    menu:
        "Tiga":
            jump toko_wrong_answer
        "Lima":
            jump toko_wrong_answer
        "Tujuh":
            jump toko_question_three

#Third question at the shop
label toko_question_three:
    "Penjaga Toko" "Lalu selanjutnya dia pesan apa lagi?"

    menu:
        "Kentang":
            jump toko_wrong_answer
        "Permen":
            jump toko_wrong_answer
        "Biskuit":
            jump toko_question_four

label toko_question_four:
    "Penjaga Toko" "Hmm, ya, biskuit. Berapa pak?"

    menu:
        "Satu":
            jump toko_wrong_answer
        "Dua":
            jump toko_question_five
        "Tiga":
            jump toko_wrong_answer

label toko_question_five:
    "Penjaga Toko" "Baiklah. Selanjutnya, apa lagi?"

    menu:
        "Keju":
            jump toko_wrong_answer
        "Kacang":
            jump toko_question_six
        "Pisang":
            jump toko_wrong_answer

label toko_question_six:
    "Penjaga Toko" "Kebetulan kami punya banyak kacang sekarang ini. Berapa banyak yang dia mau?"

    menu:
        "Satu":
            jump toko_question_seven
        "Dua":
            jump toko_wrong_answer
        "Tiga":
            jump toko_wrong_answer

label toko_question_seven:
    "Penjaga Toko" "Oke. Kacang. Nah, masih ada lagi?"

    menu:
        "Sudah. Hanya itu saja.":
            jump toko_wrong_answer
        "Empat ekor bandeng.":
            jump toko_wrong_answer
        "Empat lembar dendeng.":
            jump toko_right_answer

#When wrong answer at the shop
label toko_wrong_answer:
    las surprised "Err, Dubh? Rasanya bukan itu pesanan si kurir."
    las "Coba lagi."
    jump toko
#When right answer at the shop
label toko_right_answer:
    $ kurir_progress = 2
    "Penjaga Toko" "Haha! Baiklah!"
    "Penjaga Toko" "Maaf aku harus bertanya sepanjang itu."
    "Penjaga Toko" "Kemarin ini ada berandalan yang mengaku-ngaku hendak mengambil pesanan, ternyata membohongiku."
    "Penjaga Toko" "Sayangnya aku tidak berhasil menemukan si berandalan itu."
    "Penjaga Toko" "Ini semua pesanannya. Hati-hati membawanya!"
    jump toko

#Interact with oil bottle at Dubh's House
label minyak_text:
    $ kincir_progress = 3
    "Dubh mengambil sedikit minyak dari wadahnya."
    jump rumahdubh

#Speak with mentor
label mentor_text:
    
    if mentor_progress == 0:
        $ mentor_progress = 1
        "Mentor" "Oh? Kalian berdua."
        "Mentor" "Ada apa?"
        las normal "Kami kemari untuk minta surat rekomendasi dari Bapak."
        "Mentor" "Bukannya kemarin ini sudah ...?"
        las "Kali ini untuk melamar magang menjadi Scavenger."
        "Mentor" "Heh. Aku tidak heran melihatmu berminat menjadi Scavenger, Lasta, tapi Dubh?"
        "Mentor" "Jangan seret-seret dia terus kemanapun kau pergi dong."
        dub sad "Saya ... tidak diterima jadi Guardian, Pak ...."
        las sad "Dan saya tidak diterima jadi Scout."
        "Mentor" "...."
        "Mentor" "Astaga. Baiklah kalau begitu."
        "Mentor" "Kubuatkan surat rekomendasi kalian, tapi bantu aku sedikit."
        "Mentor" "Rematikku kambuh dan ayam-ayamku lepas."
        "Mentor" "Tolong tangkap mereka lalu masukkan ke kandang."
        las normal "Siap, Pak."

        #go to chicken catch screen
        scene bg halaman with fade
        call screen chicken_screen()

    elif mentor_progress == 2:
        $ mentor_progress = 999
        "Mentor" "Bagaimana?"
        las normal "Beres, Pak!"
        "Mentor" "Aah, kalian berdua memang bisa diandalkan!"
        "Mentor" "Sebentar, stempel di sini dan ... yak!"
        "Mentor" "Silakan."
        "Mentor menyerahkan dua surat rekomendasi pada Dubh dan Lasta."
        "Stempel lilin biru di bagian bawah surat terlihat masih basah."
        las smirk "Terima kasih banyak!"
        las "Dan semoga cepat sembuh, Pak!"
        dub normal "...."
        "Mentor" "Kenapa?"
        dub "...."
        dub "Kata Ayah, mandi air panas bagus untuk mengurangi rematik."
        "Mentor" "Heheh, terima kasih banyak atas sarannya."
        "Mentor" "Kalau susah bergerak begini, mau memanaskan air juga sebetulnya sulit ...."
        "Mentor" "Yah, tapi, terima kasih."
    elif mentor_progress == 999:
        "Mentor" "Hati-hati di ibukota dan hemat uang kalian."
        "Mentor" "Semoga sukses!"
    jump rumahmentor

#Speak with courier
label kurir_text:
    if kurir_progress == 0:
        $ kurir_progress = 1
        "Kurir" "?"
        "Kurir" "Ada perlu apa denganku?"
        las normal "Selamat siang, Paman!"
        "Kurir" "... Aku bukan pamanmu."
        "Kurir" "Langsung saja, ada perlu apa?"
        las "Kami perlu pergi ke ibukota dalam waktu dekat. Apa masih ada tempat di kereta Paman kalau kami ingin menumpang?"
        "Kurir" "...."
        las "...."
        "Kurir" "Masih."
        las upset "Um ... dan kami boleh menumpang?"
        "Kurir" "Ya. Boleh."
        las "...."
        "Kurir" "...."
        las sad "Paman mau kami membayar berapa ...?"
        "Kurir" "Tidak usah bayar."
        "Kurir" "Bantu aku mengambil beberapa barang pesananku dari toko saja."
        "Kurir" "Bilang kalian mau mengambil pesanan Tiomanach, kurir."
        "Kurir" "Kalau ditanya apa pesanannya, aku pesan tujuh kantung air bersih, dua pak biskuit, satu bungkus kacang, dan empat lembar dendeng."
        "Kurir" "Paham?"
        las normal "Kami akan segera kembali kalau begitu."
    elif kurir_progress == 1:
        "Kurir" "Lupa apa pesananku?"
        "Kurir" "Tujuh kantung air bersih, dua pak biskuit, satu bungkus kacang, dan empat lembar dendeng."

    elif kurir_progress == 2:
        $ kurir_progress = 999
        las normal "Ini pesanan Paman semuanya."
        "Sang kurir memeriksa barang-barang yang dibawakan Dubh dan Lasta, kemudian mengangguk puas."
        "Kurir" "Bagus."
        las "...."
        "Kurir" "...."
        las surprised "Ada lagi yang bisa kami bantu?"
        "Kurir" "Itu saja. Terima kasih."
        "Kurir" "Aku berangkat besok lusa. Pagi-pagi."
        "Kurir" "Begitu gerbang kota dibuka, aku akan berangkat."
        "Kurir" "Salah kalian kalau sampai terlambat dan ketinggalan."
        "Kurir" "Paham?"
        las normal "Mengerti, Paman."
    elif kurir_progress == 999:
        "Kurir" "Aku berangkat besok lusa. Segera setelah gerbang kota dibuka."
        "Kurir" "Jangan terlambat."
    jump persimpangan
##########



label endch1:
    show dubh normal at midleft with dissolve
    show lasta normal at midright with dissolve
    "Ibu" "Sudah selesai semuanya?"
    "Dubh mengangguk, otomatis menoleh ke arah Lasta di sebelahnya seakan hendak mengatakan bahwa sebagian besar urusan hari itu lancar berkat si Vulpes."
    las normal "Hehe, tidak masalah."
    las "Sudah hampir jam makan siang, jadi kurasa aku pulang dulu."
    las "Memastikan Ibu makan, lalu kembali lanjut bekerja di toko."
    "Ibu" "Kebetulan pai dagingnya baru matang."
    las surprised "Ohhh! Pantas ada bau wang--"
    las "Err, maaf."
    "Ibu" "Kubawakan sedikit untukmu kalau begitu."
    las "Tunggu--! Tidak usah!"
    las "Bibi! Jangan repot-repot!"
    "Dubh langsung meraih kerah baju Lasta dan mencengkeram ikat pinggangnya dengan tangan satu lagi, mencegah Lasta kabur."
    las upset "Dubh!"
    "Ibu" "Bagus, pegangi dia. Jangan sampai kabur."
    "Ibu" "Kaukira aku tidak tahu sejak pengurus rumah kalian pensiun, masakanmu tidak ada yang beres?"
    "Ibu" "Pegangi dia, jangan sampai lepas, Dubh."
    "Lasta mengeluarkan dengkingan protes, membuat Dubh sekalian melingkarkan lengan di sekeliling leher si Vulpes."
    las sad "Ibu akan mengira aku merampok makan siang kalian ...."
    dub normal "Tidak bakalan."
    "Tidak sampai lima menit, ibu Dubh kembali dan menyodorkan sebuah keranjang pada Lasta."
    "Ibu" "Masih panas. Jangan dihabiskan tengah jalan, ya."
    "Ibu" "Kalau ada waktu, aku mau-mau saja mengajarimu memasak, lho. Datang saja."
    las normal "Terima kasih banyak, Bibi."
    las "Aku pulang dulu kalau begitu."
    las "Sampai ketemu ... nanti. Atau besok."
    dub smile "Atau lusa."
    las smirk "Atau lusa."
    hide lasta with moveoutright
    show dubh normal at mcenter with move
    "Dubh mengawasi sahabat berbulu merahnya sampai sosoknya menghilang di ujung jalan sebelum menutup pintu rumahnya."
    dub sad "Ibu Lasta ... tidak apa-apa ditinggal?"
    "Ibu" "Anak itu mungkin sudah minta tolong seseorang untuk menjaga ibunya selama dia pergi, tapi ...."
    "Ibu" "Kurasa aku akan mampir menengoknya."
    "Ibu" "Nah, sekarang, mari kita makan dulu."
    hide dubh with dissolve
    stop music fadeout 3.0

    "-Akhir Demo 1 Dubh&Lasta-"
    "Terima kasih sudah mencoba!"

    scene black with fade

    $ renpy.full_restart(transition=False, label='_invoke_main_menu', target='_main_menu')