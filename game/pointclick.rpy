##### CHICKEN SCREEN #####

#Captured chicken tracker
define chicken_1 = False
define chicken_2 = False
define chicken_3 = False
define chicken_4 = False
define chicken_5 = False

define chicken_counter = 0

screen chicken_screen():
    if not chicken_1:
        imagebutton:
            xanchor 0.5
            yanchor 0.5
            xpos 200
            ypos 225
            idle "chicken.png"
            action [SetVariable("chicken_counter", chicken_counter+1), SetVariable("chicken_1", True)]

    if not chicken_2:
        imagebutton:
            xanchor 0.5
            yanchor 0.5
            xpos 800
            ypos 225
            idle "chicken.png"
            action [SetVariable("chicken_counter", chicken_counter+1), SetVariable("chicken_2", True)]

    if not chicken_3:
        imagebutton:
            xanchor 0.5
            yanchor 0.5
            xpos 430
            ypos 125
            idle "chicken.png"
            action [SetVariable("chicken_counter", chicken_counter+1), SetVariable("chicken_3", True)]

    if not chicken_4:
        imagebutton:
            xanchor 0.5
            yanchor 0.5
            xpos 500
            ypos 325
            idle "chicken.png"
            action [SetVariable("chicken_counter", chicken_counter+1), SetVariable("chicken_4", True)]

    if not chicken_5:
        imagebutton:
            xanchor 0.5
            yanchor 0.5
            xpos 100
            ypos 425
            idle "chicken.png"
            action [SetVariable("chicken_counter", chicken_counter+1), SetVariable("chicken_5", True)]

    if chicken_counter == 5:
        textbutton "Semua ayam ditemukan!" xalign 0.5 yalign 0.5 action Jump("chicken_finish")

label chicken_finish:
    scene bg rumahmentor with fade
    $ mentor_progress = 2
    jump rumahmentor
##########