# The script of the game goes in this file.

##### IMAGE POSITION #####
define midright = Position(xpos=.75, ypos=1.35, xanchor='center')
define mcenter = Position(xpos=.5, ypos=1.35, xanchor='center')
define midleft = Position(xpos=.25, ypos=1.35, xanchor='center')

##### CHARACTERS #####
### Dubh
define dub = Character('Dubh', image = "dubh", who_color="#ffffff")

layeredimage dubh:

    always:
        "dubh_base"

    group face:
        pos (167, 131)
        attribute normal
        attribute sad
        attribute smile
        attribute hesitated
        attribute angry

image side dubh = LayeredImageProxy("dubh", Transform(crop=(97, 69, 250, 250)))
    

### Lasta
define las = Character('Lasta', image = "lasta", who_color="#ffffff")

layeredimage lasta:

    always:
        "lasta_base"

    group face:
        pos (121, 108)
        attribute normal
        attribute sad
        attribute smirk
        attribute surprised
        attribute upset

image side lasta = LayeredImageProxy("lasta", Transform(crop=(52, 24, 250, 250)))

### NPC CHARACTERS ###

### Penjaga Kota
define pen = Character('Penjaga Kota', who_color="#ffffff")

### London
define lon = Character('London', who_color="#ffffff")

### 

##########

##### AUDIO #####
### BGM
define audio.assassin = "audio/bgm/Assassins LOOP PERCUSSION.ogg"
define audio.barren = "audio/bgm/Barren Settlement.ogg"
define audio.lurking = "audio/bgm/Lurking LOOP.ogg"

define audio.market = "audio/bgm/Medieval Market LOOP.ogg"
define audio.cave = "audio/bgm/Pretty Cave.ogg"
define audio.tavern = "audio/bgm/Tavern LOOP LIVELY.ogg"

define audio.tense = "audio/bgm/Tense Combat LOOP.ogg"
define audio.upbeat = "audio/bgm/Upbeat City LOOP.ogg"
define audio.village = "audio/bgm/Village.ogg"

### SFX
define audio.crow = "audio/sfx/ANIMAL_Bird_Crow_01_mono.wav"
define audio.tiger = "audio/sfx/ANIMAL_Tiger_Growl_03.wav"
define audio.magic = "audio/sfx/MAGIC_SPELL_Shield_mono.wav"
define audio.thud = "audio/sfx/THUD_Subtle_Tap_mono.wav"

##########

##### ADDITIONAL UI #####
# ATL to dissolve imagebutton in "hidden object section"
init -2:
    transform obj_fade:
        alpha 1.0
        easein 1.0 alpha 0.0

# To show map button
screen map_btn(map_name=None):
    if map_name:
        vbox xalign 1.0 yalign 0.0:
            if not renpy.get_screen("say"):
                imagebutton:
                    idle "compass.png"
                    action Call(map_name)

# Language change
label language_chooser:
    scene black
    menu:
        "Bahasa Indonesia":
            $ renpy.change_language(None)
        "English":
            $ renpy.change_language("english")

    return

##########

##### PREFERENCES #####
default preferences.text_cps = 50

##########

# The game starts here.

label start:
    ### Location tracker variable
    $ curr_location = "undefined"


    jump ch1

    
    
